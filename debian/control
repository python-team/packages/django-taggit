Source: django-taggit
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders:
 Michal Čihař <nijel@debian.org>,
Section: python
Priority: optional
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-python3,
 dh-sequence-sphinxdoc,
 gettext,
Build-Depends-Indep:
 python-django-doc <!nodoc>,
 python3-all,
 python3-doc <!nodoc>,
 python3-django <!nocheck>,
 python3-djangorestframework <!nocheck>,
 python3-setuptools,
 python3-sphinx <!nodoc>,
 python3-sphinx-rtd-theme <!nodoc>,
Rules-Requires-Root: no
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/python-team/packages/django-taggit
Vcs-Git: https://salsa.debian.org/python-team/packages/django-taggit.git
Homepage: https://github.com/jazzband/django-taggit

Package: python-django-taggit-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends:
 ${misc:Depends},
 ${sphinxdoc:Depends},
Suggests:
 python-django-doc,
 python3-doc,
Description: simple tagging for Django (Documentation)
 This is a generic tagging application for Django, which allows
 association of a number of tags with any Model instance and makes
 retrieval of tags simple.
 .
 django-taggit a simpler approach to tagging with Django. Add "taggit" to your
 INSTALLED_APPS then just add a TaggableManager to your model.
 .
 This package contains the documentation.

Package: python3-django-taggit
Architecture: all
Depends:
 python3-django (>= 3:4.1),
 ${misc:Depends},
 ${python3:Depends},
Breaks:
 ${python3:Breaks},
Suggests: python-django-taggit-doc
Description: simple tagging for Django (Python 3)
 This is a generic tagging application for Django, which allows
 association of a number of tags with any Model instance and makes
 retrieval of tags simple.
 .
 django-taggit a simpler approach to tagging with Django. Add "taggit" to your
 INSTALLED_APPS then just add a TaggableManager to your model.
 .
 This package installs the library for Python 3.
